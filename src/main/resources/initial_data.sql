INSERT INTO coffeedrink(coffeedrinkname) VALUES ('Cafe Creme');
INSERT INTO coffeedrink(coffeedrinkname) VALUES ('Cappuccino');
INSERT INTO coffeedrink(coffeedrinkname) VALUES ('Americano');

INSERT INTO unitofmeasurement(unitOfMeasurementName) VALUES ('teaspoon');
INSERT INTO unitofmeasurement(unitOfMeasurementName) VALUES ('tablespoon');
INSERT INTO unitofmeasurement(unitOfMeasurementName) VALUES ('fl oz');
INSERT INTO unitofmeasurement(unitOfMeasurementName) VALUES ('ml');
INSERT INTO unitofmeasurement(unitOfMeasurementName) VALUES ('cup');
INSERT INTO unitofmeasurement(unitOfMeasurementName) VALUES ('shot');

INSERT INTO drinkingredient (drinkingredientcost, drinkingredientname, unitofmeasurement_unitofmeasurementid) VALUES (0.35, 'Whole Milk', 5);
INSERT INTO drinkingredient (drinkingredientcost, drinkingredientname, unitofmeasurement_unitofmeasurementid) VALUES (0.50, 'Espresso', 6);
INSERT INTO drinkingredient (drinkingredientcost, drinkingredientname, unitofmeasurement_unitofmeasurementid) VALUES (0.12, 'Sugar', 5);
INSERT INTO drinkingredient (drinkingredientcost, drinkingredientname, unitofmeasurement_unitofmeasurementid) VALUES (0.36, 'Whipped Cream', 5);
INSERT INTO drinkingredient (drinkingredientcost, drinkingredientname, unitofmeasurement_unitofmeasurementid) VALUES (0.03, 'Simple Syrup', 1);
INSERT INTO drinkingredient (drinkingredientcost, drinkingredientname, unitofmeasurement_unitofmeasurementid) VALUES (0.27, 'Steamed Milk', 6);
INSERT INTO drinkingredient (drinkingredientcost, drinkingredientname, unitofmeasurement_unitofmeasurementid) VALUES (0.27, 'Half & Half', 6);
INSERT INTO drinkingredient (drinkingredientcost, drinkingredientname, unitofmeasurement_unitofmeasurementid) VALUES (0.27, 'Foamed Milk', 6);
INSERT INTO drinkingredient (drinkingredientcost, drinkingredientname, unitofmeasurement_unitofmeasurementid) VALUES (0.27, 'Heavy Cream', 6);
INSERT INTO drinkingredient (drinkingredientcost, drinkingredientname, unitofmeasurement_unitofmeasurementid) VALUES (0.01, 'Hot Water', 5);
INSERT INTO drinkingredient (drinkingredientcost, drinkingredientname, unitofmeasurement_unitofmeasurementid) VALUES (0.01, 'Chocolate', 6);

INSERT INTO allergen (allergenName) VALUES ('Peanut');
INSERT INTO allergen (allergenName) VALUES ('Tree Nuts');
INSERT INTO allergen (allergenName) VALUES ('Milk');
INSERT INTO allergen (allergenName) VALUES ('Egg');
INSERT INTO allergen (allergenName) VALUES ('Wheat');
INSERT INTO allergen (allergenName) VALUES ('Soy');
INSERT INTO allergen (allergenName) VALUES ('Fish');
INSERT INTO allergen (allergenName) VALUES ('Shellfish');

INSERT INTO bakerygoodcategory (bakerygoodcategoryname) VALUES ('Cake');
INSERT INTO bakerygoodcategory (bakerygoodcategoryname) VALUES ('Pie');
INSERT INTO bakerygoodcategory (bakerygoodcategoryname) VALUES ('Pastry');
INSERT INTO bakerygoodcategory (bakerygoodcategoryname) VALUES ('Bar');

INSERT INTO bakerygood (bakerygoodcost, bakerygoodname, bakerygoodvendor, bakerygoodcategory_bakerygoodcategoryid) VALUES ('3.75', 'Chocolate Cake', 'Piece of Cake', 1);
INSERT INTO bakerygood (bakerygoodcost, bakerygoodname, bakerygoodvendor, bakerygoodcategory_bakerygoodcategoryid) VALUES ('0.65', 'Crescent', 'Le Petit Patisserie', 3);
INSERT INTO bakerygood (bakerygoodcost, bakerygoodname, bakerygoodvendor, bakerygoodcategory_bakerygoodcategoryid) VALUES ('0.70', 'Organic Oat Cashew Bar', 'Grow-op Co-op', 4);

INSERT INTO ingredientinrecipe (ingredientinrecipeamount, drinkingredient_drinkingredientid, coffeedrinkrecipe_coffeedrinkid) VALUES (1, 2, 2)
INSERT INTO ingredientinrecipe (ingredientinrecipeamount, drinkingredient_drinkingredientid, coffeedrinkrecipe_coffeedrinkid) VALUES (1, 6, 2)
INSERT INTO ingredientinrecipe (ingredientinrecipeamount, drinkingredient_drinkingredientid, coffeedrinkrecipe_coffeedrinkid) VALUES (1, 8, 2)

INSERT INTO ingredientinrecipe (ingredientinrecipeamount, drinkingredient_drinkingredientid, coffeedrinkrecipe_coffeedrinkid) VALUES (1, 2, 3)
INSERT INTO ingredientinrecipe (ingredientinrecipeamount, drinkingredient_drinkingredientid, coffeedrinkrecipe_coffeedrinkid) VALUES (2, 10, 3)

INSERT INTO ingredientinrecipe (ingredientinrecipeamount, drinkingredient_drinkingredientid, coffeedrinkrecipe_coffeedrinkid) VALUES (1, 2, 1)
INSERT INTO ingredientinrecipe (ingredientinrecipeamount, drinkingredient_drinkingredientid, coffeedrinkrecipe_coffeedrinkid) VALUES (1, 4, 1)