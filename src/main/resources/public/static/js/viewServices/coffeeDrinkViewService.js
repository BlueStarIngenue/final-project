

var expressO = angular.module('expressO');

expressO.service('coffeeDrinkService', function($http) {
	
	this.add = function(coffeeDrink) {
		return $http.post('/expressO/CoffeeDrink', coffeeDrink);
	}
	
	this.update = function(coffeeDrink) {
		return $http.put('/expressO/CoffeeDrink', coffeeDrink);
	}
	
	this.remove = function(coffeeDrinkId) {
		return $http.delete('/expressO/CoffeeDrink/'+ coffeeDrinkId);
	}
	
	this.list = function() {
		return $http.get('/expressO/CoffeeDrink').then(function(response) {
			return response.data;
			});
	}
	
});