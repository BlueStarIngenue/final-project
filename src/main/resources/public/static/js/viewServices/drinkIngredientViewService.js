

var expressO = angular.module('expressO');

expressO.service('drinkIngredientService', function($http) {

	this.add = function(drinkIngredient) {
		return $http.post('/expressO/DrinkIngredient', drinkIngredient);
	}
	
	this.update = function(drinkIngredient) {
		return $http.put('/expressO/DrinkIngredient', drinkIngredient);
	}
	
	this.remove = function(drinkIngredientId) {
		return $http.delete('/expressO/DrinkIngredient/'+ drinkIngredientId);
	}

	this.list = function() {
		return $http.get('/expressO/DrinkIngredient').then(function(response) {
			return response.data;
		});
	}

});

expressO.service('unitOfMeasurementService', function($http) {
	this.list = function() {
		return $http.get('/expressO/UnitOfMeasurement').then(function(response) {
			return response.data;
		});
	}

});