

var expressO = angular.module('expressO');

expressO.service('bakeryGoodViewService', function($http) {

	this.add = function(bakeryGood) {
		return $http.post('/expressO/BakeryGood', bakeryGood);
	}
	this.update = function(bakeryGood) {
		return $http.put('/expressO/BakeryGood', bakeryGood);
	}

	this.remove = function(bakeryGoodId) {
		return $http.delete('/expressO/BakeryGood/'+ bakeryGoodId);
	}
	this.list = function() {
		return $http.get('/expressO/BakeryGood').then(function(response) {
			return response.data;
		});
	}

});

expressO.service('allergenService', function($http) {
	this.list = function() {
		return $http.get('/expressO/Allergen').then(function(response) {
			return response.data;
		});
	}

});

expressO.service('bakeryGoodCategoryService', function($http) {
	this.list = function() {
		return $http.get('/expressO/BakeryGoodCategory').then(function(response) {
			return response.data;
		});
	}

});