var expressO = angular.module('expressO', ['ngRoute','ui.bootstrap', 'xeditable', 'toastr']);

expressO.config(function($routeProvider, $locationProvider, toastrConfig) {
	
	angular.extend(toastrConfig, {
	    autoDismiss: false,
	    containerId: 'toast-container',
	    maxOpened: 0,    
	    newestOnTop: true,
	    positionClass: 'toast-bottom-center',
	    preventDuplicates: false,
	    preventOpenDuplicates: false,
	    target: 'body'
	  });
	
	$routeProvider

	.when('/', {
		templateUrl : 'views/main.html',
		controller  : 'mainController'
	})

	.when('/recipes', {
		templateUrl : 'views/recipes.html',
		controller  : 'coffeeDrinkController'
	})

	.when('/ingredients', {
		templateUrl : 'views/ingredients.html',
		controller  : 'drinkIngredientController'
	})

	.when('/addRecipe', {
		templateUrl : 'views/addRecipe.html',
		controller  : 'addCoffeeDrinkRecipeController'
	})

	.when('/bakeryGoods', {
		templateUrl : 'views/bakeryGoods.html',
		controller  : 'bakeryGoodController'
	})

	.when('/addBakeryGood', {
		templateUrl : 'views/addBakeryGood.html',
		controller  : 'addBakeryGoodController'
	})

	.when('/addIngredient', {
		templateUrl : 'views/addIngredient.html',
		controller  : 'addDrinkIngredientController'
	});

});

expressO.run(function(editableOptions) {
	editableOptions.theme = 'bs3';
});

expressO.controller('mainController', function($scope) {
});