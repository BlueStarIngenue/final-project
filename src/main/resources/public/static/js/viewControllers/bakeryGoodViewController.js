
var expressO = angular.module('expressO');

    expressO.controller('bakeryGoodController', function($scope, bakeryGoodViewService, allergenService, bakeryGoodCategoryService) {
        
        $scope.bakeryGoods = [];
    	
    	function listBakeryGoods () {
    		bakeryGoodViewService.list().then(function(bakeryGoods) {
    			$scope.bakeryGoods = bakeryGoods;	
    		});
    	};
    	
    	listBakeryGoods ();
        
        allergenService.list().then(function(allergens) {
        	$scope.allergens = allergens;	
        	});
        
        bakeryGoodCategoryService.list().then(function(bakeryGoodCategories){
        	$scope.bakeryGoodCategories = bakeryGoodCategories;
        });
        
    	$scope.saveBakeryGood = function (bakeryGood) {
    		bakeryGoodViewService.update(bakeryGood);
    	};

    	$scope.removeBakeryGood = function (bakeryGood) {
    		bakeryGoodViewService.remove(bakeryGood.bakeryGoodId).then(function() {
    			listBakeryGoods();
    		})
    	};
    	
    	$scope.markAllergenForRemoval = function (allergen) {
    		allergen.deleted = true;
    	};
    	
    	$scope.addNewAllergen = function(bakeryGood) {
    		var allergen = {
    				allergen: null,
    				isNew: true
    		};
    		bakeryGood.allergens.push(allergen);
    	};

    	var unmarkAllergenForRemoval = function (bakeryGood) {
    		for (var i=0; i <bakeryGood.allergens.length; i++) {
    			bakeryGood.allergens[i].deleted = false;
    		}
    	};
    	
    	$scope.cancel = function(bakeryGood) {
    		unmarkAllergenForRemoval(bakeryGood);
    		removeNewAllergen(bakeryGood);
    	};

    	var removeNewAllergen = function (bakeryGood) {
    		for (var i = bakeryGood.allergens.length; i--;) {
    			var allergen = bakeryGood.allergens[i];     
    			if (allergen.isNew) {
    				bakeryGood.allergens.splice(i, 1);
    			}
    		}      
    	};

    	
    	$scope.checkForRedundantAllergen = function(formData, inputData) {
    		return function(allergen) {
    			var selectedAllergens = Object.keys(formData)
    			.filter(function(key) { return key.indexOf('allergen') === 0; })
    			.map(function(key) { return formData[key]; });

    			for (var i=0; i < selectedAllergens.length; i++) {
    				if (selectedAllergens[i] &&
    						selectedAllergens[i].allergenId === allergen.allergenId &&
    						(!inputData || inputData.allergenId !== allergen.allergenId)) {

    					return false;
    				}
    			}

    			return true;
    		}
    	};

    	$scope.saveEditedBakeryGood = function (bakeryGood) {
    		
    		for (var i = bakeryGood.allergens.length; i--;) {
    			var allergen = bakeryGood.allergens[i];     
    			if (allergen.deleted) {
    				bakeryGood.allergens.splice(i, 1);
    			}
    		}
    		
    		bakeryGoodViewService.update(bakeryGood);
    	};

        
    });
