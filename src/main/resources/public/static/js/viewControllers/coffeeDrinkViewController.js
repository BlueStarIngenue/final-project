
var expressO = angular.module('expressO');

expressO.controller('coffeeDrinkController', function($scope, coffeeDrinkService, drinkIngredientService) {

	$scope.coffeeDrinks = [];
	coffeeDrinkService.list().then(function(coffeeDrinks) {
		$scope.coffeeDrinks = coffeeDrinks;	
	});

	function listCoffeeDrinks () {
		coffeeDrinkService.list().then(function(coffeeDrinks) {
			$scope.coffeeDrinks = coffeeDrinks;	
		});
	}

	$scope.drinkIngredients = [];

	drinkIngredientService.list().then(function(drinkIngredients) {
		$scope.drinkIngredients = drinkIngredients;	
	});

	$scope.markDrinkIngredientForRemoval = function (ingredientInRecipe) {
		ingredientInRecipe.deleted = true;
	};

	$scope.checkForRedundantIngredient = function(formData, inputData) {
		return function(drinkIngredient) {
			var selectedIngredients = Object.keys(formData)
			.filter(function(key) { return key.indexOf('drinkIngredient') === 0; })
			.map(function(key) { return formData[key]; });

			for (var i=0; i < selectedIngredients.length; i++) {
				if (selectedIngredients[i] &&
						selectedIngredients[i].drinkIngredientId === drinkIngredient.drinkIngredientId &&
						(!inputData || inputData.drinkIngredientId !== drinkIngredient.drinkIngredientId)) {

					return false;
				}
			}

			return true;
		}
	};

	$scope.addNewDrinkIngredient = function(coffeeDrink) {
		var ingredientInRecipe = {
				drinkIngredient: null,
				ingredientInRecipeAmount: null,
				isNew: true
		};
		coffeeDrink.coffeeDrinkRecipe.push(ingredientInRecipe);
	};


	listCoffeeDrinks ();

	$scope.cancel = function(coffeeDrink) {
		unmarkDrinkIngredientForRemoval(coffeeDrink);
		removeNewDrinkIngredient(coffeeDrink);
	};

	var unmarkDrinkIngredientForRemoval = function (coffeeDrink) {
		for (var i=0; i <coffeeDrink.coffeeDrinkRecipe.length; i++) {
			coffeeDrink.coffeeDrinkRecipe[i].deleted = false;
		}
	};

	var removeNewDrinkIngredient = function (coffeeDrink) {
		for (var i = coffeeDrink.coffeeDrinkRecipe.length; i--;) {
			var ingredientInRecipe = coffeeDrink.coffeeDrinkRecipe[i];     
			if (ingredientInRecipe.isNew) {
				coffeeDrink.coffeeDrinkRecipe.splice(i, 1);
			}
		}      
	};
	
	$scope.removeCoffeeDrink = function (coffeeDrink) {
		coffeeDrinkService.remove(coffeeDrink.coffeeDrinkId).then(function() {
			listCoffeeDrinks();
		})
	};
	
	$scope.saveEditedCoffeeDrink = function (coffeeDrink) {
		
		for (var i = coffeeDrink.coffeeDrinkRecipe.length; i--;) {
			var ingredientInRecipe = coffeeDrink.coffeeDrinkRecipe[i];     
			if (ingredientInRecipe.deleted) {
				coffeeDrink.coffeeDrinkRecipe.splice(i, 1);
			}
		}
		
		coffeeDrinkService.update(coffeeDrink);
	};


});
