var expressO = angular.module('expressO');

expressO.controller('addDrinkIngredientController', function($scope, drinkIngredientService, unitOfMeasurementService, $location) {

	$scope.drinkIngredient = {drinkIngredientName: '', unitOfMeasurement: null};
	$scope.unitsOfMeasurement = [];
	
	unitOfMeasurementService.list().then(function(unitsOfMeasurement) {
    	$scope.unitsOfMeasurement = unitsOfMeasurement;	
    	});
	
	$scope.save = function() {
		drinkIngredientService.add($scope.drinkIngredient);
		$location.path("/ingredients");
		location.reload();
	};
});