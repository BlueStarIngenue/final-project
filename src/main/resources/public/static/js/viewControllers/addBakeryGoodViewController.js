var expressO = angular.module('expressO');

expressO.controller('addBakeryGoodController', function($scope, bakeryGoodViewService, allergenService, bakeryGoodCategoryService, $location) {

	$scope.bakeryGood = {bakeryGoodName: '', allergens: [], bakeryGoodCategory: null};
	$scope.allergens = [];
	$scope.allergen = {allergen: null};

	allergenService.list().then(function(allergens) {
		$scope.allergens = allergens;	
	});

	bakeryGoodCategoryService.list().then(function(bakeryGoodCategories) {
		$scope.bakeryGoodCategories = bakeryGoodCategories;
	});

	$scope.save = function() {
		bakeryGoodViewService.add($scope.bakeryGood).then(function() {
			$location.path("/bakeryGoods");
			location.reload();
		});
	};

	$scope.addNewAllergen = function() {
		$scope.bakeryGood.allergens.push($scope.allergen.allergen);
	}

	$scope.removeAllergen = function(allergen) {
		var allergenPosition = $scope.bakeryGood.allergens.indexOf(allergen);
		$scope.bakeryGood.allergens.splice(allergenPosition, 1);	
	}

	$scope.checkForRedundantAllergen = function(allergen) {
		var allergenPosition = $scope.bakeryGood.allergens.indexOf(allergen);
		if (allergenPosition == -1) {
			return true;
		}
		return false;
	}
});