var expressO = angular.module('expressO');

expressO.controller('addCoffeeDrinkRecipeController', function($scope, coffeeDrinkService, drinkIngredientService, $location) {

	$scope.coffeeDrink = {coffeeDrinkName: '', coffeeDrinkRecipe: []};

	$scope.drinksIngredients = [];
	$scope.drinkIngredient = {drinkIngredient: null};
	$scope.ingredientInRecipeAmount = {ingredientInRecipeAmount: null};

	drinkIngredientService.list().then(function(drinkIngredients) {
		$scope.drinkIngredients = drinkIngredients;	
	});

	$scope.addNewDrinkIngredient = function() {

		var ingredientInRecipe = {drinkIngredient: $scope.drinkIngredient.drinkIngredient, ingredientInRecipeAmount: $scope.ingredientInRecipeAmount.ingredientInRecipeAmount}
		$scope.coffeeDrink.coffeeDrinkRecipe.push(ingredientInRecipe);
	};

	$scope.removeDrinkIngredient = function(ingredientInRecipe) {

		var ingredientPosition = -1;
		for (var i=0; i<$scope.coffeeDrink.coffeeDrinkRecipe.length; i++) {
			if ($scope.coffeeDrink.coffeeDrinkRecipe[i] == ingredientInRecipe ) {
				ingredientPosition = i;
			}
		}
		$scope.coffeeDrink.coffeeDrinkRecipe.splice(ingredientPosition, 1);	
	};

	$scope.checkForRedundantIngredient = function(drinkIngredient) {
		var ingredientPosition = -1;
		for (var i=0; i<$scope.coffeeDrink.coffeeDrinkRecipe.length; i++) {
			if ($scope.coffeeDrink.coffeeDrinkRecipe[i].drinkIngredient == drinkIngredient ) {
				ingredientPosition = i;
			}
		}
		if (ingredientPosition == -1) {
			return true;
		}
		return false;
	};

	$scope.save = function() {
		coffeeDrinkService.add($scope.coffeeDrink);
		$location.path("/recipes");
		location.reload();
	}
});