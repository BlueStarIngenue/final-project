
var expressO = angular.module('expressO');

expressO.controller('drinkIngredientController', function($scope, drinkIngredientService, unitOfMeasurementService, toastr) {

	$scope.drinkIngredients = [];
	$scope.unitsOfMeasurement = [];

	function listDrinkIngredients () {
		drinkIngredientService.list().then(function(drinkIngredients) {
			$scope.drinkIngredients = drinkIngredients;	
		});
	}

	listDrinkIngredients ();

	unitOfMeasurementService.list().then(function(unitsOfMeasurement) {
		$scope.unitsOfMeasurement = unitsOfMeasurement;	
	});

	$scope.saveEditedIngredient = function (drinkIngredient) {
		drinkIngredientService.update(drinkIngredient)
	};

	$scope.removeIngredient = function (drinkIngredient) {
		drinkIngredientService.remove(drinkIngredient.drinkIngredientId).then(function() {
			listDrinkIngredients();
		}).catch(function(){
			toastr.info('This ingredient is being used in a recipe!', 'Information');
		})
	}
});
