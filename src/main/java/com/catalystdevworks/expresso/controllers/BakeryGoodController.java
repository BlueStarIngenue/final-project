package com.catalystdevworks.expresso.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.catalystdevworks.expresso.entities.BakeryGood;
import com.catalystdevworks.expresso.services.BakeryGoodService;

@RestController
public class BakeryGoodController {
	@Autowired
	private BakeryGoodService bakeryGoodService;

	public void setBakeryGoodService(BakeryGoodService bakeryGoodService) { 
		this.bakeryGoodService = bakeryGoodService;
	}

	@RequestMapping(value = "/expressO/BakeryGood", method = RequestMethod.POST)
	public void add(@Valid @RequestBody BakeryGood bakeryGood) {
		bakeryGoodService.add(bakeryGood);
	}

	@RequestMapping(value="/expressO/BakeryGood", method=RequestMethod.PUT)
	public void update(@Valid @RequestBody BakeryGood bakeryGood){ 
		bakeryGoodService.update(bakeryGood);
	}
	
	@RequestMapping(value="/expressO/BakeryGood", method=RequestMethod.GET)
	public List<BakeryGood> list(){
		return bakeryGoodService.listAllBakeryGoods();
	}
	
	@RequestMapping(value="/expressO/BakeryGood/{bakeryGoodId}", method=RequestMethod.DELETE)
	public void delete(@PathVariable Integer bakeryGoodId){ 
		bakeryGoodService.delete(bakeryGoodId);
	}

}
