package com.catalystdevworks.expresso.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.catalystdevworks.expresso.entities.CoffeeDrink;
import com.catalystdevworks.expresso.services.CoffeeDrinkService;

@RestController
public class CoffeeDrinkController {
	@Autowired
	private CoffeeDrinkService coffeeDrinkService;

	public void setCoffeeDrinkService(CoffeeDrinkService coffeeDrinkService) { 
		this.coffeeDrinkService = coffeeDrinkService;
	}

	@RequestMapping(value = "/expressO/CoffeeDrink", method = RequestMethod.POST)
	public void add(@Valid @RequestBody CoffeeDrink coffeeDrink) {
		coffeeDrinkService.add(coffeeDrink);
	}

	@RequestMapping(value="/expressO/CoffeeDrink", method=RequestMethod.PUT)
	public void update(@Valid @RequestBody CoffeeDrink coffeeDrink){ 
		coffeeDrinkService.update(coffeeDrink);
	}
	
	@RequestMapping(value="/expressO/CoffeeDrink", method=RequestMethod.GET)
	public List<CoffeeDrink> list(){
		return coffeeDrinkService.listAllCoffeeDrinks();
	}
	
	@RequestMapping(value="/expressO/CoffeeDrink/{coffeeDrinkId}", method=RequestMethod.DELETE)
	public void delete(@PathVariable Integer coffeeDrinkId){ 
		coffeeDrinkService.delete(coffeeDrinkId);
	}

}
