package com.catalystdevworks.expresso.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.catalystdevworks.expresso.entities.UnitOfMeasurement;
import com.catalystdevworks.expresso.services.UnitOfMeasurementService;

@RestController
public class UnitOfMeasurementController {
	@Autowired
	private UnitOfMeasurementService unitOfMeasurementService;

	public void setUnitOfMeasurementService(UnitOfMeasurementService unitOfMeasurementService) { 
		this.unitOfMeasurementService = unitOfMeasurementService;
	}
	
	@RequestMapping(value="/expressO/UnitOfMeasurement", method=RequestMethod.GET)
	public List<UnitOfMeasurement> list(){
		return unitOfMeasurementService.listAllUnitsOfMeasurement();
	}
	
}
