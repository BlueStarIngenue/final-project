package com.catalystdevworks.expresso.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.catalystdevworks.expresso.entities.BakeryGoodCategory;
import com.catalystdevworks.expresso.services.BakeryGoodCategoryService;

@RestController
public class BakeryGoodCategoryController {
	@Autowired
	private BakeryGoodCategoryService bakeryGoodCategoryService;

	public void setBakeryGoodCategoryService(BakeryGoodCategoryService bakeryGoodCategoryService) { 
		this.bakeryGoodCategoryService = bakeryGoodCategoryService;
	}
	
	@RequestMapping(value="/expressO/BakeryGoodCategory", method=RequestMethod.GET)
	public List<BakeryGoodCategory> list(){
		return bakeryGoodCategoryService.listAllBakeryGoodCategories();
	}
}
