package com.catalystdevworks.expresso.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.catalystdevworks.expresso.entities.Allergen;
import com.catalystdevworks.expresso.services.AllergenService;

@RestController
public class AllergenController {
	@Autowired
	private AllergenService allergenService;

	public void setAllergenService(AllergenService allergenService) { 
		this.allergenService = allergenService;
	}
	
	@RequestMapping(value="/expressO/Allergen", method=RequestMethod.GET)
	public List<Allergen> list(){
		return allergenService.listAllAllergens();
	}
}
