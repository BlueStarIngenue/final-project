package com.catalystdevworks.expresso.controllers;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.catalystdevworks.expresso.entities.DrinkIngredient;
import com.catalystdevworks.expresso.services.DrinkIngredientService;

@RestController
public class DrinkIngredientController {
	@Autowired
	private DrinkIngredientService drinkIngredientService;

	public void setCoffeeDrinkService(DrinkIngredientService drinkIngredientService) { 
		this.drinkIngredientService = drinkIngredientService;
	}

	@RequestMapping(value = "/expressO/DrinkIngredient", method = RequestMethod.POST)
	public void add(@Valid @RequestBody DrinkIngredient drinkIngredient) {
		drinkIngredientService.add(drinkIngredient);
	}

	@RequestMapping(value="/expressO/DrinkIngredient", method=RequestMethod.PUT)
	public void update(@Valid @RequestBody DrinkIngredient drinkIngredient){ 
		drinkIngredientService.update(drinkIngredient);
	}
	
	@RequestMapping(value="/expressO/DrinkIngredient", method=RequestMethod.GET)
	public List<DrinkIngredient> list(){
		return drinkIngredientService.listAllDrinkIngredients();
	}
	
	@RequestMapping(value="/expressO/DrinkIngredient/{drinkIngredientId}", method=RequestMethod.DELETE)
	public void delete(@PathVariable Integer drinkIngredientId){ 
		drinkIngredientService.delete(drinkIngredientId);
	}


}
