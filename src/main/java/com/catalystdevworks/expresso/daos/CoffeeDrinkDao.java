package com.catalystdevworks.expresso.daos;

import java.util.List;

import com.catalystdevworks.expresso.entities.CoffeeDrink;

public interface CoffeeDrinkDao {
	
	public List<CoffeeDrink> getAllCoffeeDrinks();

	public void add(CoffeeDrink coffeeDrink);
	
	public void update(CoffeeDrink coffeeDrink);
	
	public void delete(Integer coffeeDrinkId);
	
	public CoffeeDrink getCoffeeDrinkById(Integer coffeeDrinkId);

	public CoffeeDrink getCoffeeDrinkByName(String coffeDrinkName);

}
