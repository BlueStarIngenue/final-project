package com.catalystdevworks.expresso.daos;

import java.util.List;

import com.catalystdevworks.expresso.entities.Allergen;

public interface AllergenDao {

	public List<Allergen> getAllAllergens();
}
