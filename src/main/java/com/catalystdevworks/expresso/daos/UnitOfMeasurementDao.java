package com.catalystdevworks.expresso.daos;

import java.util.List;

import com.catalystdevworks.expresso.entities.UnitOfMeasurement;


public interface UnitOfMeasurementDao {

	public List<UnitOfMeasurement> getAllUnitsOfMeasurement();

}
