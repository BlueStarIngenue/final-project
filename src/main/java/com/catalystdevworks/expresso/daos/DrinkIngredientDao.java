package com.catalystdevworks.expresso.daos;

import java.util.List;

import com.catalystdevworks.expresso.entities.DrinkIngredient;

public interface DrinkIngredientDao {
	
	public List<DrinkIngredient> getAllDrinkIngredients();

	public void add(DrinkIngredient drinkIngredient);
	
	public void update(DrinkIngredient drinkIngredient);
	
	public void delete(Integer drinkIngredientId);
	
	public DrinkIngredient getDrinkIngredientById(Integer drinkIngredientId);

	public DrinkIngredient getDrinkIngredientByName(String drinkIngredientName);

}
