package com.catalystdevworks.expresso.daos;

import java.util.List;

import com.catalystdevworks.expresso.entities.BakeryGoodCategory;

public interface BakeryGoodCategoryDao {
	public List<BakeryGoodCategory> getAllBakeryGoodCategories();
}
