package com.catalystdevworks.expresso.daos;

import com.catalystdevworks.expresso.entities.IngredientInRecipe;

public interface IngredientInRecipeDao {
	public IngredientInRecipe getIngredientInRecipeByCoffeeDrinkName(String coffeeDrinkName);
}
