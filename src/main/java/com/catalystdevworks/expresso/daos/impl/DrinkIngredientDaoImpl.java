package com.catalystdevworks.expresso.daos.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.catalystdevworks.expresso.daos.DrinkIngredientDao;
import com.catalystdevworks.expresso.entities.DrinkIngredient;

@Repository
@Transactional
public class DrinkIngredientDaoImpl implements DrinkIngredientDao {

	@PersistenceContext
	private EntityManager em;

	public void setEm(EntityManager em) {
		this.em = em;
	}

	public List<DrinkIngredient> getAllDrinkIngredients() {
		return em.createQuery("SELECT i FROM DrinkIngredient i", DrinkIngredient.class).getResultList();
	}

	public void add(DrinkIngredient drinkIngredient) {
		em.persist(drinkIngredient);

	}

	public void update(DrinkIngredient drinkIngredient) {
		em.merge(drinkIngredient);

	}

	public void delete(Integer drinkIngredientId) {
		DrinkIngredient drinkIngredient = getDrinkIngredientById(drinkIngredientId);
		em.remove(drinkIngredient);
	}

	public DrinkIngredient getDrinkIngredientById(Integer drinkIngredientId) {
		return em.createQuery("SELECT d FROM DrinkIngredient d WHERE d.drinkIngredientId = :id", DrinkIngredient.class).setParameter("id", drinkIngredientId)
		.getSingleResult();
	}

	public DrinkIngredient getDrinkIngredientByName(String drinkIngredientName) {
		List<DrinkIngredient> drinkIngredient = em.createQuery("SELECT c FROM CoffeeDrink c WHERE c.coffeeDrinkName = :coffeeDrinkName", DrinkIngredient.class)
				.setParameter("coffeeDrinkName", drinkIngredientName).getResultList();
		if (drinkIngredient.size() == 0) {
			return null;
		}
		return drinkIngredient.get(0);
	}
}
