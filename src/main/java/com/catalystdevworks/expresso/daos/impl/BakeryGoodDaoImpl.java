package com.catalystdevworks.expresso.daos.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.catalystdevworks.expresso.daos.BakeryGoodDao;
import com.catalystdevworks.expresso.entities.BakeryGood;

@Repository
@Transactional
public class BakeryGoodDaoImpl implements BakeryGoodDao {

	@PersistenceContext
	private EntityManager em;

	public void setEm(EntityManager em) {
		this.em = em;
	}

	public List<BakeryGood> getAllBakeryGoods() {
		return em.createQuery("SELECT b FROM BakeryGood b", BakeryGood.class).getResultList();
	}

	public void add(BakeryGood bakeryGood) {
		em.merge(bakeryGood);

	}

	public void update(BakeryGood bakeryGood) {
		em.merge(bakeryGood);

	}

	public void delete(Integer bakeryGoodId) {
		BakeryGood bakeryGood = getBakeryGoodById(bakeryGoodId);
		em.remove(bakeryGood);

	}

	public BakeryGood getBakeryGoodById(Integer bakeryGoodId) {
		return em.createQuery("SELECT b FROM BakeryGood b WHERE b.bakeryGoodId = :id", BakeryGood.class).setParameter("id", bakeryGoodId)
		.getSingleResult();
	}

	public BakeryGood getBakeryGoodByName(String bakeryGoodName) {
		List<BakeryGood> bakeryGood = em.createQuery("SELECT b FROM BakeryGood b WHERE b.bakeryGoodName = :bakeryGoodName", BakeryGood.class)
				.setParameter("bakeryGoodName", bakeryGoodName).getResultList();
		if (bakeryGood.size() == 0) {
			return null;
		}
		return bakeryGood.get(0);
	}
}
