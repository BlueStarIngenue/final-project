package com.catalystdevworks.expresso.daos.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.catalystdevworks.expresso.daos.AllergenDao;
import com.catalystdevworks.expresso.entities.Allergen;

@Repository
@Transactional
public class AllergenDaoImpl implements AllergenDao {

	@PersistenceContext
	private EntityManager em;

	public void setEm(EntityManager em) {
		this.em = em;
	}
	
	public List<Allergen> getAllAllergens() {
		return em.createQuery("SELECT a FROM Allergen a", Allergen.class).getResultList();
	}

}
