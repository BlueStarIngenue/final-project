package com.catalystdevworks.expresso.daos.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.catalystdevworks.expresso.daos.UnitOfMeasurementDao;
import com.catalystdevworks.expresso.entities.CoffeeDrink;
import com.catalystdevworks.expresso.entities.UnitOfMeasurement;

@Repository
@Transactional
public class UnitOfMeasurementDaoImpl implements UnitOfMeasurementDao {

	@PersistenceContext
	private EntityManager em;

	public void setEm(EntityManager em) {
		this.em = em;
	}
	
	public List<UnitOfMeasurement> getAllUnitsOfMeasurement() {
		return em.createQuery("SELECT u FROM UnitOfMeasurement u", UnitOfMeasurement.class).getResultList();
	}

}
