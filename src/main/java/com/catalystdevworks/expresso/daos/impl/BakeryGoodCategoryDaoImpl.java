package com.catalystdevworks.expresso.daos.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.catalystdevworks.expresso.daos.BakeryGoodCategoryDao;
import com.catalystdevworks.expresso.entities.BakeryGoodCategory;

@Repository
@Transactional
public class BakeryGoodCategoryDaoImpl implements BakeryGoodCategoryDao {


	@PersistenceContext
	private EntityManager em;

	public void setEm(EntityManager em) {
		this.em = em;
	}
	
	public List<BakeryGoodCategory> getAllBakeryGoodCategories() {
		return em.createQuery("SELECT a FROM BakeryGoodCategory a", BakeryGoodCategory.class).getResultList();
	}

}
