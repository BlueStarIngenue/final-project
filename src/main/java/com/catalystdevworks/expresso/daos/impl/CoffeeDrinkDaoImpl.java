package com.catalystdevworks.expresso.daos.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;


import com.catalystdevworks.expresso.daos.CoffeeDrinkDao;
import com.catalystdevworks.expresso.entities.CoffeeDrink;

@Repository
@Transactional
public class CoffeeDrinkDaoImpl implements CoffeeDrinkDao {

	@PersistenceContext
	private EntityManager em;

	public void setEm(EntityManager em) {
		this.em = em;
	}

	public List<CoffeeDrink> getAllCoffeeDrinks() {
		return em.createQuery("SELECT c FROM CoffeeDrink c", CoffeeDrink.class).getResultList();
	}

	public void add(CoffeeDrink coffeeDrink) {
		em.persist(coffeeDrink);
	}

	public void update(CoffeeDrink coffeeDrink) {
		em.merge(coffeeDrink);

	}

	public void delete(Integer coffeeDrinkId) {
		CoffeeDrink coffeeDrink = getCoffeeDrinkById(coffeeDrinkId);
		em.remove(coffeeDrink);
	}

	public CoffeeDrink getCoffeeDrinkById(Integer coffeeDrinkId) {
		return em.createQuery("SELECT c FROM CoffeeDrink c WHERE c.coffeeDrinkId = :id", CoffeeDrink.class).setParameter("id", coffeeDrinkId)
		.getSingleResult();
	}

	public CoffeeDrink getCoffeeDrinkByName(String coffeeDrinkName) {
		List<CoffeeDrink> coffeeDrink = em.createQuery("SELECT c FROM CoffeeDrink c WHERE c.coffeeDrinkName = :coffeeDrinkName", CoffeeDrink.class)
				.setParameter("coffeeDrinkName", coffeeDrinkName).getResultList();
		if (coffeeDrink.size() == 0) {
			return null;
		}
		return coffeeDrink.get(0);
	}
}
