package com.catalystdevworks.expresso.daos;

import java.util.List;

import com.catalystdevworks.expresso.entities.BakeryGood;

public interface BakeryGoodDao {
	
	public List<BakeryGood> getAllBakeryGoods();

	public void add(BakeryGood bakeryGood);
	
	public void update(BakeryGood bakeryGood);
	
	public void delete(Integer bakeryGoodId);
	
	public BakeryGood getBakeryGoodById(Integer bakeryGoodId);

	public BakeryGood getBakeryGoodByName(String bakeryGoodName);

}
