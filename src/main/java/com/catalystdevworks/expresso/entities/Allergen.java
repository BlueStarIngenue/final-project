package com.catalystdevworks.expresso.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Entity
public class Allergen {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private int allergenId;

	@NotNull
	@Pattern (regexp = ".*\\S.*" , message = "The name of the allergen should be at least one non white-space character.") //minimum of character that isn't white-space
	@Column
	private String allergenName;

	public Allergen(){

	}

	public int getAllergenId() {
		return allergenId;
	}

	public void setAllergenId(int allergenId) {
		this.allergenId = allergenId;
	}

	public String getAllergenName() {
		return allergenName;
	}

	public void setAllergenName(String allergenName) {
		this.allergenName = allergenName;
	}
	
	
}
