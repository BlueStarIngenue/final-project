package com.catalystdevworks.expresso.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Entity
public class BakeryGoodCategory {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private int bakeryGoodCategoryId;

	@NotNull
	@Pattern (regexp = ".*\\S.*" , message = "The name of the bakery good category should be at least one non white-space character.") //minimum of character that isn't white-space
	@Column
	private String bakeryGoodCategoryName;

	public BakeryGoodCategory(){

	}

	public int getBakeryGoodCategoryId() {
		return bakeryGoodCategoryId;
	}

	public void setBakeryGoodCategoryId(int bakeryGoodCategoryId) {
		this.bakeryGoodCategoryId = bakeryGoodCategoryId;
	}

	public String getBakeryGoodCategoryName() {
		return bakeryGoodCategoryName;
	}

	public void setBakeryGoodCategoryName(String bakeryGoodCategoryName) {
		this.bakeryGoodCategoryName = bakeryGoodCategoryName;
	}
	
	
}
