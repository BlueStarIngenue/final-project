package com.catalystdevworks.expresso.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Entity
public class CoffeeDrink { 
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private int coffeeDrinkId;
	
	@NotNull
	@Pattern (regexp = ".*\\S.*" , message = "The name of the drink should be at least one non white-space character.") //minimum of character that isn't white-space
	@Column
	private String coffeeDrinkName;
	
	@OneToMany (cascade = CascadeType.ALL, orphanRemoval = true)
	@NotNull
	@JoinColumn
	private List <IngredientInRecipe> coffeeDrinkRecipe;
	public CoffeeDrink(){
	}

	public List<IngredientInRecipe> getCoffeeDrinkRecipe() {
		return coffeeDrinkRecipe;
	}

	public void setCoffeeDrinkRecipe(List<IngredientInRecipe> coffeeDrinkRecipe) {
		this.coffeeDrinkRecipe = coffeeDrinkRecipe;
	}

	public int getCoffeeDrinkId() {
		return coffeeDrinkId;
	}

	public void setCoffeeDrinkId(int coffeeDrinkId) {
		this.coffeeDrinkId = coffeeDrinkId;
	}

	public String getCoffeeDrinkName() {
		return coffeeDrinkName;
	}

	public void setCoffeeDrinkName(String coffeeDrinkName) {
		this.coffeeDrinkName = coffeeDrinkName;
	} 
	
}
