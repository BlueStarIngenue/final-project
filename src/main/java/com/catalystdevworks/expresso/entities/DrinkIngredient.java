package com.catalystdevworks.expresso.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Entity
public class DrinkIngredient {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private int drinkIngredientId;
	
	@NotNull
	@Pattern (regexp = ".*\\S.*" , message = "The name of the ingre  dient should be at least one non white-space character.") //minimum of character that isn't white-space
	@Column
	private String drinkIngredientName;
	
	@NotNull
	@Min(value = 0)
	@Column
	private double drinkIngredientCost;
	
	@ManyToOne
	@NotNull
	@JoinColumn
	private UnitOfMeasurement unitOfMeasurement;

	public DrinkIngredient(){
		
	}

	public int getDrinkIngredientId() {
		return drinkIngredientId;
	}

	public void setDrinkIngredientId(int drinkIngredientId) {
		this.drinkIngredientId = drinkIngredientId;
	}

	public String getDrinkIngredientName() {
		return drinkIngredientName;
	}

	public void setDrinkIngredientName(String drinkIngredientName) {
		this.drinkIngredientName = drinkIngredientName;
	}
	
	

	public double getDrinkIngredientCost() {
		return drinkIngredientCost;
	}

	public void setDrinkIngredientCost(double drinkIngredientCost) {
		this.drinkIngredientCost = drinkIngredientCost;
	}

	public UnitOfMeasurement getUnitOfMeasurement() {
		return unitOfMeasurement;
	}

	public void setUnitOfMeasurement(UnitOfMeasurement unitOfMeasurement) {
		this.unitOfMeasurement = unitOfMeasurement;
	} 
	
	
	
}
