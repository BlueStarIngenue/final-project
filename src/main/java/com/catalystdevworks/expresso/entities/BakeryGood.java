package com.catalystdevworks.expresso.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Entity
public class BakeryGood {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private int bakeryGoodId;
	
	@NotNull
	@Pattern (regexp = ".*\\S.*" , message = "The name of the item should be at least one non white-space character.") //minimum of character that isn't white-space
	@Column
	private String bakeryGoodName;
	
	@NotNull
	@Pattern (regexp = ".*\\S.*" , message = "The name of the vendor should be at least one non white-space character.") //minimum of character that isn't white-space
	@Column
	private String bakeryGoodVendor;
	
	@NotNull
	@Min(value = 0)
	@Column
	private double bakeryGoodCost;
	
	@ManyToMany (cascade = CascadeType.ALL)
	@JoinTable
	private List<Allergen> allergens;
	
	@ManyToOne
	@NotNull
	@JoinColumn
	private BakeryGoodCategory bakeryGoodCategory;

	public int getBakeryGoodId() {
		return bakeryGoodId;
	}

	public void setBakeryGoodId(int bakeryGoodId) {
		this.bakeryGoodId = bakeryGoodId;
	}

	public String getBakeryGoodName() {
		return bakeryGoodName;
	}

	public void setBakeryGoodName(String bakeryGoodName) {
		this.bakeryGoodName = bakeryGoodName;
	}

	public String getBakeryGoodVendor() {
		return bakeryGoodVendor;
	}

	public void setBakeryGoodVendor(String bakeryGoodVendor) {
		this.bakeryGoodVendor = bakeryGoodVendor;
	}

	public double getBakeryGoodCost() {
		return bakeryGoodCost;
	}

	public void setBakeryGoodCost(double bakeryGoodCost) {
		this.bakeryGoodCost = bakeryGoodCost;
		
	}

	public List<Allergen> getAllergens() {
		return allergens;
	}

	public void setAllergens(List<Allergen> allergens) {
		this.allergens = allergens;
	}

	public BakeryGoodCategory getBakeryGoodCategory() {
		return bakeryGoodCategory;
	}

	public void setBakeryGoodCategory(BakeryGoodCategory bakeryGoodCategory) {
		this.bakeryGoodCategory = bakeryGoodCategory;
	}
	
	
	
}
