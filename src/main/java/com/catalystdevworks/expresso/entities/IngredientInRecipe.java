package com.catalystdevworks.expresso.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Entity
public class IngredientInRecipe {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private int ingredientInRecipeId;
	
	@NotNull
	@Min(value = 0)
	@Column
	private double ingredientInRecipeAmount;

	@ManyToOne
	@NotNull
	@JoinColumn
	private DrinkIngredient drinkIngredient;

	public IngredientInRecipe(){

	}

	public int getIngredientInRecipeId() {
		return ingredientInRecipeId;
	}

	public void setIngredientInRecipeId(int ingredientInRecipeId) {
		this.ingredientInRecipeId = ingredientInRecipeId;
	}

	public double getIngredientInRecipeAmount() {
		return ingredientInRecipeAmount;
	}

	public void setIngredientInRecipeAmount(double ingredientInRecipeAmount) {
		this.ingredientInRecipeAmount = ingredientInRecipeAmount;
	}

	public DrinkIngredient getDrinkIngredient() {
		return drinkIngredient;
	}

	public void setDrinkIngredient(DrinkIngredient drinkIngredient) {
		this.drinkIngredient = drinkIngredient;
	}

}
