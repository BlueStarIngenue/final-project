package com.catalystdevworks.expresso.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Entity
public class UnitOfMeasurement {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private int unitOfMeasurementId;

	@NotNull
	@Pattern (regexp = ".*\\S.*" , message = "The name of the ingredient should be at least one non white-space character.") //minimum of character that isn't white-space
	@Column
	private String unitOfMeasurementName;

	public UnitOfMeasurement(){

	}

	public int getUnitOfMeasurementId() {
		return unitOfMeasurementId;
	}

	public void setUnitOfMeasurementId(int unitOfMeasurementId) {
		this.unitOfMeasurementId = unitOfMeasurementId;
	}

	public String getUnitOfMeasurementName() {
		return unitOfMeasurementName;
	}

	public void setUnitOfMeasurementName(String unitOfMeasurementName) {
		this.unitOfMeasurementName = unitOfMeasurementName;
	}
	
}
