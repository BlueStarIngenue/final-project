package com.catalystdevworks.expresso.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.catalystdevworks.expresso.daos.AllergenDao;
import com.catalystdevworks.expresso.entities.Allergen;
import com.catalystdevworks.expresso.services.AllergenService;

@Service
public class AllergenServiceImpl implements AllergenService {
	
	@Autowired
	private AllergenDao allergenDao;

	public void setAllergenDao(AllergenDao allergenDao) {
		this.allergenDao = allergenDao;
	}

	public List<Allergen> listAllAllergens() {
		return allergenDao.getAllAllergens();
	}

}
