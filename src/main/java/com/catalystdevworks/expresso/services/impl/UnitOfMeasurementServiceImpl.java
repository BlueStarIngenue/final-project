package com.catalystdevworks.expresso.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.catalystdevworks.expresso.daos.UnitOfMeasurementDao;
import com.catalystdevworks.expresso.entities.UnitOfMeasurement;
import com.catalystdevworks.expresso.services.UnitOfMeasurementService;

@Service
public class UnitOfMeasurementServiceImpl implements UnitOfMeasurementService {

	@Autowired
	private UnitOfMeasurementDao unitOfMeasurementDao;

	public void setUnitOfMeasurementDao(UnitOfMeasurementDao unitOfMeasurementDao) {
		this.unitOfMeasurementDao = unitOfMeasurementDao;
	}

	public List<UnitOfMeasurement> listAllUnitsOfMeasurement() {
		return unitOfMeasurementDao.getAllUnitsOfMeasurement();
	}

}
