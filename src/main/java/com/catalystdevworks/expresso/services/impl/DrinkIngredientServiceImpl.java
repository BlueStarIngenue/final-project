package com.catalystdevworks.expresso.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.catalystdevworks.expresso.daos.CoffeeDrinkDao;
import com.catalystdevworks.expresso.daos.DrinkIngredientDao;
import com.catalystdevworks.expresso.entities.DrinkIngredient;
import com.catalystdevworks.expresso.services.DrinkIngredientService;

@Service
public class DrinkIngredientServiceImpl implements DrinkIngredientService {

	@Autowired
	private DrinkIngredientDao drinkIngredientDao;

	public void setDrinkIngredientDao(DrinkIngredientDao drinkIngredientDao) {
		this.drinkIngredientDao = drinkIngredientDao;
	}
	
	public void update(DrinkIngredient drinkIngredient) {
		drinkIngredientDao.update(drinkIngredient);

	}

	public void add(DrinkIngredient drinkIngredient) {
		drinkIngredientDao.add(drinkIngredient);

	}

	public void delete(Integer drinkIngredientId) {
		drinkIngredientDao.delete(drinkIngredientId);

	}

	public List<DrinkIngredient> listAllDrinkIngredients() {
		return drinkIngredientDao.getAllDrinkIngredients();
	}

}
