package com.catalystdevworks.expresso.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.catalystdevworks.expresso.daos.BakeryGoodDao;
import com.catalystdevworks.expresso.entities.BakeryGood;
import com.catalystdevworks.expresso.services.BakeryGoodService;

@Service
public class BakeryGoodServiceImpl implements BakeryGoodService {

	@Autowired
	private BakeryGoodDao bakeryGoodDao;

	public void setBakeryGoodDao(BakeryGoodDao bakeryGoodDao) {
		this.bakeryGoodDao = bakeryGoodDao;
	}

	public void update(BakeryGood bakeryGood) {
		bakeryGoodDao.update(bakeryGood);

	}

	public void add(BakeryGood bakeryGood) {
		bakeryGoodDao.add(bakeryGood);

	}

	public void delete(Integer bakeryGoodId) {
		bakeryGoodDao.delete(bakeryGoodId);

	}

	public List<BakeryGood> listAllBakeryGoods() {
		return bakeryGoodDao.getAllBakeryGoods();
	}

}
