package com.catalystdevworks.expresso.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.catalystdevworks.expresso.daos.BakeryGoodCategoryDao;
import com.catalystdevworks.expresso.entities.BakeryGoodCategory;
import com.catalystdevworks.expresso.services.BakeryGoodCategoryService;

@Service
public class BakeryGoodCategoriesServiceImpl implements BakeryGoodCategoryService {

	@Autowired
	private BakeryGoodCategoryDao bakeryGoodCategoryDao;

	public void setBakeryGoodCategoryDao(BakeryGoodCategoryDao bakeryGoodCategoryDao) {
		this.bakeryGoodCategoryDao = bakeryGoodCategoryDao;
	}
	
	public List<BakeryGoodCategory> listAllBakeryGoodCategories() {
		return bakeryGoodCategoryDao.getAllBakeryGoodCategories();
	}

}
