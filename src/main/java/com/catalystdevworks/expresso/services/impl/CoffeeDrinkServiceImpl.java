package com.catalystdevworks.expresso.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.catalystdevworks.expresso.daos.CoffeeDrinkDao;
import com.catalystdevworks.expresso.entities.CoffeeDrink;
import com.catalystdevworks.expresso.services.CoffeeDrinkService;

@Service
public class CoffeeDrinkServiceImpl implements CoffeeDrinkService {

	@Autowired
	private CoffeeDrinkDao coffeeDrinkDao;

	public void setCoffeeDrinkDao(CoffeeDrinkDao coffeeDrinkDao) {
		this.coffeeDrinkDao = coffeeDrinkDao;
	}

	public void update(CoffeeDrink coffeeDrink) {
		coffeeDrinkDao.update(coffeeDrink);

	}


	public void add(CoffeeDrink coffeeDrink) {
		coffeeDrinkDao.add(coffeeDrink);

	}

	public void delete(Integer coffeeDrinkId) {
		coffeeDrinkDao.delete(coffeeDrinkId);

	}

	public List<CoffeeDrink> listAllCoffeeDrinks() {
		return coffeeDrinkDao.getAllCoffeeDrinks();

	}

}
