package com.catalystdevworks.expresso.services;

import java.util.List;

import com.catalystdevworks.expresso.entities.BakeryGood;

public interface BakeryGoodService {
	
	void update(BakeryGood bakeryGood);

	void add(BakeryGood bakeryGood);
	
	void delete(Integer bakeryGoodId);

	List<BakeryGood> listAllBakeryGoods();
}
