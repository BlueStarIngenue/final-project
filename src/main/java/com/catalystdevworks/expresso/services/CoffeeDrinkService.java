package com.catalystdevworks.expresso.services;

import java.util.List;

import com.catalystdevworks.expresso.entities.CoffeeDrink;

public interface CoffeeDrinkService {
	
	void update(CoffeeDrink coffeeDrink);

	void add(CoffeeDrink coffeeDrink);
	
	void delete(Integer coffeeDrinkId);

	List<CoffeeDrink> listAllCoffeeDrinks();
}
