package com.catalystdevworks.expresso.services;

import java.util.List;

import com.catalystdevworks.expresso.entities.DrinkIngredient;

public interface DrinkIngredientService {
	void update(DrinkIngredient drinkIngredient);

	void add(DrinkIngredient drinkIngredient);
	
	void delete(Integer drinkIngredientId);

	List<DrinkIngredient> listAllDrinkIngredients();
}
