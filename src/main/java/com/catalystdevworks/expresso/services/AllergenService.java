package com.catalystdevworks.expresso.services;

import java.util.List;

import com.catalystdevworks.expresso.entities.Allergen;

public interface AllergenService {
	
	List<Allergen> listAllAllergens();
}
