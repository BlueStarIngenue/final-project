package com.catalystdevworks.expresso.services;

import java.util.List;

import com.catalystdevworks.expresso.entities.BakeryGoodCategory;

public interface BakeryGoodCategoryService {
	
	List<BakeryGoodCategory> listAllBakeryGoodCategories();

}
