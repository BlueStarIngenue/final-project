package com.catalystdevworks.expresso.services;

import java.util.List;

import com.catalystdevworks.expresso.entities.UnitOfMeasurement;

public interface UnitOfMeasurementService {
	
	List<UnitOfMeasurement> listAllUnitsOfMeasurement();
}
