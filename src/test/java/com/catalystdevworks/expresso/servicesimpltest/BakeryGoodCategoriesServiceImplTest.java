package com.catalystdevworks.expresso.servicesimpltest;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.Before;
import org.junit.Test;

import com.catalystdevworks.expresso.daos.BakeryGoodCategoryDao;
import com.catalystdevworks.expresso.services.impl.BakeryGoodCategoriesServiceImpl;

public class BakeryGoodCategoriesServiceImplTest {
	private BakeryGoodCategoriesServiceImpl target;
	private BakeryGoodCategoryDao mockBakeryGoodCategoryDao;

	@Before
	public void setup() {
		target = new BakeryGoodCategoriesServiceImpl();
		mockBakeryGoodCategoryDao = mock(BakeryGoodCategoryDao.class);
		target.setBakeryGoodCategoryDao(mockBakeryGoodCategoryDao);
	}

	@Test
	public void testGetAllBakeryGoodCategories() {
		target.listAllBakeryGoodCategories();
		verify(mockBakeryGoodCategoryDao, times(1)).getAllBakeryGoodCategories();
	}
}
