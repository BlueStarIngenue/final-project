package com.catalystdevworks.expresso.servicesimpltest;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.Before;
import org.junit.Test;

import com.catalystdevworks.expresso.daos.DrinkIngredientDao;
import com.catalystdevworks.expresso.entities.DrinkIngredient;
import com.catalystdevworks.expresso.services.impl.DrinkIngredientServiceImpl;

public class DrinkIngredientServiceImplTest {
	private DrinkIngredientDao mockDrinkIngredientDao;
	private DrinkIngredientServiceImpl target;

	@Before
	public void setup() {
		target = new DrinkIngredientServiceImpl();
		mockDrinkIngredientDao = mock(DrinkIngredientDao.class);
		target.setDrinkIngredientDao(mockDrinkIngredientDao);
	}
	
	@Test
	public void testUpdateDrinkIngredient() {
		DrinkIngredient drinkIngredient = new DrinkIngredient();
		target.update(drinkIngredient);
		verify(mockDrinkIngredientDao, times(1)).update(drinkIngredient);
	}
	
	@Test
	public void testAddDrinkIngredient() throws Exception {
		DrinkIngredient drinkIngredient = new DrinkIngredient();
		target.add(drinkIngredient);
		verify(mockDrinkIngredientDao, times(1)).add(drinkIngredient);
	}
	
	@Test
	public void testDeleteDrinkIngredient() throws Exception {
		target.delete(1);
		verify(mockDrinkIngredientDao, times(1)).delete(1);
	}


	@Test
	public void testListAllDrinkIngredients() {
		target.listAllDrinkIngredients();
		verify(mockDrinkIngredientDao, times(1)).getAllDrinkIngredients();
	}

}
