package com.catalystdevworks.expresso.servicesimpltest;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.Before;
import org.junit.Test;

import com.catalystdevworks.expresso.daos.BakeryGoodDao;
import com.catalystdevworks.expresso.entities.BakeryGood;
import com.catalystdevworks.expresso.services.impl.BakeryGoodServiceImpl;

public class BakeryGoodServiceImplTest {
	private BakeryGoodDao mockBakeryGoodDao;
	private BakeryGoodServiceImpl target;

	@Before
	public void setup() {
		target = new BakeryGoodServiceImpl();
		mockBakeryGoodDao = mock(BakeryGoodDao.class);
		target.setBakeryGoodDao(mockBakeryGoodDao);
	}
	
	@Test
	public void testUpdateBakeryGood() {
		BakeryGood bakeryGood = new BakeryGood();
		target.update(bakeryGood);
		verify(mockBakeryGoodDao, times(1)).update(bakeryGood);
	}
	
	@Test
	public void testAddBakeryGood() throws Exception {
		BakeryGood bakeryGood = new BakeryGood();
		target.add(bakeryGood);
		verify(mockBakeryGoodDao, times(1)).add(bakeryGood);
	}
	
	@Test
	public void testDeleteBakeryGood() throws Exception {
		target.delete(1);
		verify(mockBakeryGoodDao, times(1)).delete(1);
	}


	@Test
	public void testListAllBakeryGoods() {
		target.listAllBakeryGoods();
		verify(mockBakeryGoodDao, times(1)).getAllBakeryGoods();
	}

}
