package com.catalystdevworks.expresso.servicesimpltest;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.Before;
import org.junit.Test;

import com.catalystdevworks.expresso.daos.CoffeeDrinkDao;
import com.catalystdevworks.expresso.entities.CoffeeDrink;
import com.catalystdevworks.expresso.services.impl.CoffeeDrinkServiceImpl;

public class CoffeeDrinkServiceImplTest {
	private CoffeeDrinkDao mockCoffeeDrinkDao;
	private CoffeeDrinkServiceImpl target;

	@Before
	public void setup() {
		target = new CoffeeDrinkServiceImpl();
		mockCoffeeDrinkDao = mock(CoffeeDrinkDao.class);
		target.setCoffeeDrinkDao(mockCoffeeDrinkDao);
	}
	
	@Test
	public void testUpdateCoffeeDrink() {
		CoffeeDrink coffeeDrink = new CoffeeDrink();
		target.update(coffeeDrink);
		verify(mockCoffeeDrinkDao, times(1)).update(coffeeDrink);
	}
	
	@Test
	public void testAddCoffeeDrink() throws Exception {
		CoffeeDrink coffeeDrink = new CoffeeDrink();
		target.add(coffeeDrink);
		verify(mockCoffeeDrinkDao, times(1)).add(coffeeDrink);
	}
	
	@Test
	public void testDeleteCoffeeDrink() throws Exception {
		target.delete(1);
		verify(mockCoffeeDrinkDao, times(1)).delete(1);
	}


	@Test
	public void testListAllCoffeeDrinks() {
		target.listAllCoffeeDrinks();
		verify(mockCoffeeDrinkDao, times(1)).getAllCoffeeDrinks();
	}

	
}
