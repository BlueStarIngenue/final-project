package com.catalystdevworks.expresso.servicesimpltest;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.Before;
import org.junit.Test;

import com.catalystdevworks.expresso.daos.AllergenDao;
import com.catalystdevworks.expresso.services.impl.AllergenServiceImpl;

public class AllergenServiceImplTest {
	private AllergenServiceImpl target;
	private AllergenDao mockAllergenDao;

	@Before
	public void setup() {
		target = new AllergenServiceImpl();
		mockAllergenDao = mock(AllergenDao.class);
		target.setAllergenDao(mockAllergenDao);
	}

	@Test
	public void testGetAllAllergens() {
		target.listAllAllergens();
		verify(mockAllergenDao, times(1)).getAllAllergens();
	}
}
