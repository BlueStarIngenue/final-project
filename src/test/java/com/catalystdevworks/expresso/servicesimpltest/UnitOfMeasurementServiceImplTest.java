package com.catalystdevworks.expresso.servicesimpltest;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.Before;
import org.junit.Test;

import com.catalystdevworks.expresso.daos.UnitOfMeasurementDao;
import com.catalystdevworks.expresso.services.impl.UnitOfMeasurementServiceImpl;

public class UnitOfMeasurementServiceImplTest {
	private UnitOfMeasurementDao mockUnitOfMeasurementDao;
	private UnitOfMeasurementServiceImpl target;

	@Before
	public void setup() {
		target = new UnitOfMeasurementServiceImpl();
		mockUnitOfMeasurementDao = mock(UnitOfMeasurementDao.class);
		target.setUnitOfMeasurementDao(mockUnitOfMeasurementDao);
	}

	@Test
	public void testListAllUnitsOfMeasurement() {
		target.listAllUnitsOfMeasurement();
		verify(mockUnitOfMeasurementDao, times(1)).getAllUnitsOfMeasurement();
	}

}
