package com.catalystdevworks.expresso.seleniumtest;

import java.util.List;
import java.util.Random;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class IngredientSeleniumTest {

	private WebDriver driver;

	@Before
	public void setUp() {
		System.setProperty("webdriver.chrome.driver", "C:/Tools/chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("http://localhost:8080");
	}

	@Test
	public void testAddIngredientRecipe() {
		String drinkIngredientName = generateString();
		createDrinkIngredient(drinkIngredientName);
		new WebDriverWait(driver, 30).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[contains(text(),'Whipped Cream')]")));
		new WebDriverWait(driver, 30).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[contains(text(),'" + drinkIngredientName + "')]")));
		List<WebElement> list = driver.findElements(By.xpath("//*[contains(text(),'" + drinkIngredientName + "')]"));
		Assert.assertTrue("Text not found!", list.size() > 0);
	}
	
	@After
	public void quitDriver() throws Exception {
		driver.quit();
	}

	private void createDrinkIngredient(String drinkIngredientName) {
		new WebDriverWait(driver, 30).until(ExpectedConditions.presenceOfElementLocated(By.id("drinkIngredientLink"))).click();
		new WebDriverWait(driver, 30).until(ExpectedConditions.presenceOfElementLocated(By.id("addIngredientLink"))).click();
		new WebDriverWait(driver, 30).until(ExpectedConditions.presenceOfElementLocated(By.id("ingredientNameInput"))).sendKeys(drinkIngredientName);
		new Select(driver.findElement(By.id("select"))).selectByVisibleText("cup");
		new WebDriverWait(driver, 30).until(ExpectedConditions.presenceOfElementLocated(By.id("ingredientCostInput"))).sendKeys("1");			
		new WebDriverWait(driver, 30).until(ExpectedConditions.presenceOfElementLocated(By.id("addIngredient"))).click();
	}

	public String generateString() {
		Random rng = new Random();
		String characters = "abcdefghijklmnopqrstuvwxyz";
		int length = 8;
		char[] text = new char[length];
		for (int i = 0; i < length; i++) {
			text[i] = characters.charAt(rng.nextInt(characters.length()));
		}
		return new String(text);
	}
}
