package com.catalystdevworks.expresso.seleniumtest;

import java.util.List;
import java.util.Random;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CoffeeDrinkRecipeSeleniumTest {

	private WebDriver driver;

	@Before
	public void setUp() {
		System.setProperty("webdriver.chrome.driver", "C:/Tools/chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("http://localhost:8080");
	}

	@Test
	public void testAddCoffeeDrinkRecipe() {
		String coffeeDrinkName = generateString();
		createCoffeeDrinkRecipe(coffeeDrinkName);
		new WebDriverWait(driver, 30).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[contains(text(),'Cappuccino')]")));
		List<WebElement> list = driver.findElements(By.xpath("//*[contains(text(),'" + coffeeDrinkName + "')]"));
		Assert.assertTrue("Text not found!", list.size() > 0);
	}
	
	@After
	public void quitDriver() throws Exception {
		driver.quit();
	}

	private void createCoffeeDrinkRecipe(String coffeeDrinkName) {
		new WebDriverWait(driver, 30).until(ExpectedConditions.presenceOfElementLocated(By.id("drinkRecipesLink"))).click();
		new WebDriverWait(driver, 30).until(ExpectedConditions.presenceOfElementLocated(By.id("addRecipeLink"))).click();
		new WebDriverWait(driver, 30).until(ExpectedConditions.presenceOfElementLocated(By.id("coffeeDrinkNameInput"))).sendKeys(coffeeDrinkName);
		new Select(driver.findElement(By.id("select"))).selectByVisibleText("Whole Milk");
		new WebDriverWait(driver, 30).until(ExpectedConditions.presenceOfElementLocated(By.id("ingredientInRecipeAmount"))).sendKeys("1");			
		new WebDriverWait(driver, 30).until(ExpectedConditions.presenceOfElementLocated(By.id("addIngredient"))).click();
		new WebDriverWait(driver, 30).until(ExpectedConditions.presenceOfElementLocated(By.id("addRecipe"))).click();
	}

	public String generateString() {
		Random rng = new Random();
		String characters = "abcdefghijklmnopqrstuvwxyz";
		int length = 8;
		char[] text = new char[length];
		for (int i = 0; i < length; i++) {
			text[i] = characters.charAt(rng.nextInt(characters.length()));
		}
		return new String(text);
	}
}
